Installation

    composer install
    rm -rf app/cache/*
    rm -rf app/logs/*
    setfacl -d -m u:www-data:rwx,u:`whoami`:rwx app/cache/ app/logs/
    setfacl -m u:www-data:rwX,u:`whoami`:rwX app/cache/ app/logs/
    bower install