<?php

namespace LolMe\LolMeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('lol_me');

        $rootNode
            ->children()
            ->scalarNode('key')->isRequired()->end()
            ->scalarNode('default_region')->defaultValue('euw')->end();

        return $treeBuilder;
    }
}