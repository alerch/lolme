<?php

namespace LolMe\LolMeBundle\Controller;

use LeagueWrap\Api;
use LolMe\LolMeBundle\Services\ApiService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class TestController extends Controller
{
    /**
     * @Route("/", name="startpage")
     * @Template
     */
    public function indexAction()
    {
        /** @var Api $api */
        $api = $this->get('lolApi')->getClient();
        $api->setRegion('euw');
        try {

            $summoner = $api->summoner()->info('I3re4k');
        } catch (\Exception $e) {
            var_dump($e);
            die();
        }



        return ['summoner' => $summoner];
    }
}
