<?php

namespace LolMe\LolMeBundle\Services;

use LeagueWrap\Api;

class ApiService
{
    protected $key;
    protected $client;

    public function __construct($key, $region)
    {
        $this->key = $key;

        $this->client = new Api($this->key);
        $this->client->setRegion($region);
    }

    public function getClient()
    {
        return $this->client;
    }
}